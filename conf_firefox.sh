#!/bin/bash

DISPLAY=:1 firefox &

sleep 1s

for pid in $(pidof firefox)
do
kill $pid
done

cat firefox.conf >> $(find .mozilla/firefox/ -name prefs.js)
