
FROM centos:7

RUN yum install -y epel-release dnf \
        && \
        dnf install -y \
            tigervnc-server \
            openbox obconf-qt \
            lxqt-about lxqt-common lxqt-config lxqt-globalkeys lxqt-notificationd \
            lxqt-openssh-askpass lxqt-panel lxqt-policykit lxqt-qtplugin lxqt-runner \
            lxqt-session pcmanfm-qt \
            dejavu-sans-mono-fonts \
            xterm nano htop expect sudo \
        && \
        yum clean all && dnf clean all \
        && \
        rm -rf /var/cache/yum/* && rm -rf /var/cache/dnf/*

ENV HOME=/home/headless/
ENV password='centos'

RUN /bin/dbus-uuidgen --ensure && \
        useradd headless && \
        echo "centos" | passwd --stdin root && \
        echo "centos" | passwd --stdin headless


COPY ./startup.sh ${HOME}
RUN mkdir -p ${HOME}/.vnc \
        && \
        echo '#!/bin/sh' > ${HOME}/.vnc/xstartup && \
        echo 'exec startlxqt' >> ${HOME}/.vnc/xstartup && \
        chmod 775 ${HOME}/.vnc/xstartup \
        && \
        chown headless:headless -R ${HOME}
COPY qwe.txt start.sh start_tor.sh conf_firefox.sh firefox.conf ${HOME}
RUN cat ${HOME}/qwe.txt > /etc/yum.repos.d/tor.repo 
RUN yum install -y tor && yum install -y firefox &&\
    chown -R headless:headless /run/tor &&\
    chown headless:headless ${HOME}start.sh &&\
    chown headless:headless ${HOME}start_tor.sh &&\
    chown headless:headless ${HOME}startup.sh &&\
    chown headless:headless ${HOME}conf_firefox.sh &&\
    chown headless:headless ${HOME}firefox.conf
    
EXPOSE 5901
WORKDIR ${HOME}
USER headless
ENTRYPOINT ["/usr/bin/bash"]
CMD ["start.sh"]
